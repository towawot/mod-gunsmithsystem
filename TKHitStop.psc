ScriptName TKHitStop Hidden

float Function GetGlobalTimeMultiplier() global native
Function SetGlobalTimeMultiplier(float fTime) global native
Function PauseGame(bool bPause) global native
bool Function IsGamePaused() global native
