Scriptname GunsmithSystem hidden

; FUNCTIONS
Function BlockAttackRelease(bool bBlock) global native
Function BlockAttackStart(bool bBlock) global native
Function BlockReloadStart(bool bBlock) global native
bool Function IsBlockAttackRelease() global native
bool Function IsBlockAttackStart() global native
bool Function IsBlockReloadStart() global native
