Scriptname GSSController extends Form  

int   property maxBullets = 30 auto
{max clip size. 0 = unlimited.}
bool  property isAutomatic = false auto

float property firingSpeed = 0.1 auto

float property reloadTime = 2.0 auto

float[] Function GetWeaponData()
	float[] results
	results = new float[4]
	results[0] = maxBullets as float
	results[1] = isAutomatic as float
	results[2] = firingSpeed
	results[3] = ReloadTime
	return results
endFunction

