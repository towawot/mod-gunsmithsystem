Scriptname GSSMainScript extends ReferenceAlias  

int		_curBullets

int		_maxBullets
bool	_isAutomatic
float	_firingSpeed
float	_reloadTime

bool	DetectRecoil
float[]	RecoilData

weapon	weap
actor	aPlayer
int		CROSSBOW = 9
int		AttackKey

bool	IsReadyFire
bool	durAutoShot
bool	durLoop

Import GunsmithSystem

Event OnInit()
	aPlayer = Game.GetPlayer()
endEvent

Event OnPlayerLoadGame()
	aPlayer = Game.GetPlayer()
	Form fmObject = aPlayer.GetEquippedObject(0)
	if (fmObject)
		if (fmObject.HasKeywordString("GunsmithSystem"))
			InitGSS(fmObject)
			DetectRecoil = IsRecoilLoaded()
			return
		endif
	endif
	ResetGSS()
endEvent

Event OnObjectUnEquipped(Form akBaseObject, ObjectReference akReference)
	if (akBaseObject as Weapon)
		if (akBaseObject.HasKeywordString("GunsmithSystem"))
			Form fmObject = aPlayer.GetEquippedObject(0)
			if (fmObject)
				if (!fmObject.HasKeywordString("GunsmithSystem"))
					ResetGSS()
				endif
			endif
		endif
	endif
endEvent


Event OnObjectEquipped(Form akBaseObject, ObjectReference akReference)
	if (akBaseObject as Weapon)
		if (akBaseObject.HasKeywordString("GunsmithSystem"))
			InitGSS(akBaseObject)
			DetectRecoil = IsRecoilLoaded()
		endif
	endIf
endEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (asEventName == "bowDrawn")
		IsReadyFire = true
		_curBullets = _maxBullets
	elseif (asEventName == "attackStop")
		IsReadyFire = false
		durLoop = false
	endif
endEvent

Event OnKeyDown(Int KeyCode)
	if (!Utility.IsInMenuMode())
		if (KeyCode == AttackKey) 
			Form fmObject = aPlayer.GetEquippedObject(0)
			if (fmObject)
				if (fmObject.HasKeywordString("GunsmithSystem"))
					if (_isAutomatic)
						if (weap && IsReadyFire && !durLoop)
							durAutoShot = true
							AutoShot()
						endif
					else
						if (weap && IsReadyFire)
							Shot()
							if (_curBullets <= 0 && _maxBullets > 0)
								NoAmmo()
							endif
						endif
					endif
				endif
			endif
		endif
	endif
EndEvent

Event OnKeyUp(Int KeyCode, Float HoldTime)
	if (!Utility.IsInMenuMode())
		Form fmObject = aPlayer.GetEquippedObject(0)
		if (fmObject)
			if (fmObject.HasKeywordString("GunsmithSystem"))
				if (KeyCode == AttackKey && _isAutomatic)
					durAutoShot = false
				endif
			endif
		endif
	endif
EndEvent

Event OnUpdate()
	if (IsBlockAttackStart())
		BlockAttackStart(false)
	endif
endEvent

Function Shot()
	weap.fire(aPlayer)
	_curBullets -= 1
	if (DetectRecoil && RecoilData[0])
		Recoilshot(RecoilData[1], RecoilData[2], RecoilData[3], RecoilData[4])
	endif
endFunction

Function AutoShot()
	while (durAutoShot && IsReadyFire)
		durLoop = true
		Shot()
		if (_curBullets <= 0 && _maxBullets > 0)
			NoAmmo()
		else
			utility.wait(_firingSpeed)
		endif
	endWhile
	durLoop = false
endFunction

Function NoAmmo()
	BlockAttackStart(true)
	if (!_reloadTime)
		RegisterForSingleUpdate(1.0)
	else
		RegisterForSingleUpdate(_reloadTime)
	endif
	Debug.SendAnimationEvent(aPlayer, "attackStop")
endFunction

Function InitGSS(Form fmObject)
	if (!fmObject)
		return
	endif
	
	weap = fmObject as Weapon
	if (!weap)
		return
	endif
	
	if (weap.GetWeaponType() == CROSSBOW)
		float[] weapData
		weapData = new float[4]
		weapData = (fmObject as GSSController).GetWeaponData()
		_maxBullets = weapData[0] as int
		_isAutomatic = weapData[1] as bool
		_firingSpeed = weapData[2]
		_reloadTime = weapData[3]

		BlockAttackRelease(true)
		BlockReloadStart(true)
		registerForAnimationEvent(aPlayer, "bowDrawn")
		registerForAnimationEvent(aPlayer, "AttackStop")

		IsReadyFire = false
		durAutoShot = false
		durLoop = false

		AttackKey = Input.GetMappedKey("Right Attack/Block")
		RegisterForKey(AttackKey)
	endif
endFunction

Function ResetGSS()
	weap = None
	BlockAttackStart(false)
	BlockAttackRelease(false)
	BlockReloadStart(false)
	UnregisterForAnimationEvent(aPlayer, "bowDrawn")
	UnregisterForAnimationEvent(aPlayer, "AttackStop")
	UnregisterForKey(AttackKey)
	IsReadyFire = false
	durAutoShot = false
	durLoop = false
endFunction

bool Function IsRecoilLoaded()
	bool result
	RecoilData = new float[5]
	if (Game.GetModByName("TKRecoil.esp") < 255)
		result = true
		RecoilData = GetRecoilSettings()
	else
		result = false
	endif
	return result
endFunction
		
float[] Function GetRecoilSettings()
	float[] results
	results = new float[5]
	results[0] = (Game.GetFormFromFile(0x01D90, "TKRecoil.esp") as GlobalVariable).GetValue()	;activate
	results[1] = (Game.GetFormFromFile(0x01D94, "TKRecoil.esp") as GlobalVariable).GetValue()	;time
	results[2] = (Game.GetFormFromFile(0x01D91, "TKRecoil.esp") as GlobalVariable).GetValue()	;blur
	results[3] = (Game.GetFormFromFile(0x01D92, "TKRecoil.esp") as GlobalVariable).GetValue()	;cam
	results[4] = (Game.GetFormFromFile(0x01D93, "TKRecoil.esp") as GlobalVariable).GetValue()	;cont
	return results
endFunction

Function RecoilShot(float Time, float Blur, float Cam, float Cont)
   if (!Game.GetPlayer().IsInKillMove())
		Game.ShakeController(Cont, Cont, 0.25)
		Game.ShakeCamera(None, Cam, 0.25)
		(Game.GetFormFromFile(0x012C9, "TKRecoil.esp") as ImageSpaceModifier).Apply(Blur)
		if (Time)
			if (!TKHitStop.IsGamePaused())
				TKHitStop.PauseGame(true)
				Utility.Wait(Time)
				TKHitStop.PauseGame(false)
			endif
		endif
   endif
EndFunction

